<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/',[HomeController::class,'home'])->name('home');
Route::get('/dashboard',[HomeController::class,'dashboard'])->name('dashboard');
Route::get('/appraisal',[HomeController::class,'appraisal'])->name('appraisal');
Route::get('/requisition',[HomeController::class,'requisition'])->name('requisition');
Route::get('/cafe-management',[HomeController::class,'cafe_management'])->name('cafe_management');
Route::get('/meal-management',[HomeController::class,'meal_management'])->name('meal_management');
Route::get('/attendance-management',[HomeController::class,'attendance_management'])->name('attendance_management');
Route::get('/leave-management',[HomeController::class,'leave_management'])->name('leave_management');
Route::get('/leave-planner',[HomeController::class,'leave_planner'])->name('leave_planner');
Route::get('/payroll-management',[HomeController::class,'payroll_management'])->name('payroll_management');
Route::get('/employee-directory',[HomeController::class,'employee_directory'])->name('employee_directory');
Route::get('/inventory',[HomeController::class,'inventory'])->name('inventory');
Route::get('/stationary-requisition',[HomeController::class,'stationary'])->name('stationary_requisition');
Route::get('/purchase-request',[HomeController::class,'purchase_request'])->name('purchase_request');