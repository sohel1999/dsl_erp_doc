<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view('common_module.home');
    }

    public function dashboard()
    {
        return view('common_module.dashboard');
    }

    public function appraisal()
    {
        return view('common_module.appraisal');
    }

    public function requisition()
    {
        return view('common_module.requisition');
    }

    public function cafe_management()
    {
        return view('common_module.cafe_management');
    }

    public function meal_management()
    {
        return view('common_module.meal_management');
    }

    public function attendance_management()
    {
        return view('common_module.attendance_management');
    }

    public function leave_management()
    {
        return view('common_module.leave_management');
    }

    public function leave_planner()
    {
        return view('common_module.leave_planner');
    }

    public function payroll_management()
    {
        return view('common_module.payroll_management');
    }

    public function employee_directory()
    {
        return view('common_module.employee_directory');
    }

    public function inventory()
    {
        return view('hr_and_admin.inventory');
    }

    public function stationary()
    {
        return view('hr_and_admin.stationary');
    }

    public function purchase_request()
    {
        return view('hr_and_admin.purchase_request');
    }

}
