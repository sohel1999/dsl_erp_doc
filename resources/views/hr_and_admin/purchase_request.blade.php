@extends('layouts.app')
@section('contain')
    <div class="content">
        <h3>New Purchase Requisition</h3>
        <p> 
            When the office runs low on stationary supplies, the admin takes the initiative to request a purchase to restock 
            those items. This ensures that the office remains equipped with the necessary stationary supplies for day-to-day 
            operations to continue smoothly.
        </p>

        <p>
            To make a request, employees first select the product name, quantity, and unit price. After completing the form, 
            they submit it and await audit approval.
        </p>
        <img src="{{asset('images/stationary/purchase_requisition.png')}}" alt="New Purchase Requisition"></br></br>
    </div>

    <div id="pending_purchase_request" class="content" style="display: none;">
        <h3>Pending Requisition</h3>
        <p>
            In this section, you can view a complete list of all your requisition requests from the day you joined until
            your last day in the office. Here, you can check the status of each requisition request, whether it is pending or approved.
        </p></br>
        <img src="{{asset('images/stationary/pending_requisition.png')}}" alt="Pending Requisition"></br></br>
    </div>


    <div id="update_purchase_request_list" class="content" style="display: none;">
        <h3>Updated Purchase Requisition List</h3>
        <p>
            After submitting the requisition form, it will be sent to the admin panel for approval. Once all approvals are completed, you will receive the stationary items you requested.
        </p>
        <img src="{{asset('images/stationary/update_purchase_request.png')}}" alt="Updated Purchase Requisition List"></br>
    </div>

    <div id="pending_update_purchase_list" class="content" style="display: none;">
        <h3>Pending Updated Purchase List</h3>
        <p>
            After submitting the requisition form, it will be sent to the admin panel for approval. Once all approvals are completed, you will receive the stationary items you requested.
        </p>
        <img src="{{asset('images/stationary/edit_purchase_requisition.png')}}" alt="Pending Updated Purchase List"></br>
    </div>

@endsection