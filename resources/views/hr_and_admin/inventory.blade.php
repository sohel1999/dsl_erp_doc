@extends('layouts.app')
@section('contain')
    <div class="content">
        <h3>Product List</h3>
        <p> 
            The Inventory Product List enables administrators to easily add products based on their requirements, 
            facilitating efficient inventory management.
        </p>
        <p>
            To add a new product, navigate to the Inventory Management section and select the "Product List" menu. 
            From there, choose "Add New Product" and fill out the required information in the form. <b>Before adding a product, 
            ensure you have set up categories, brands, units, and other necessary modules</b>, all of which are located under the 
            Inventory Management section. Once the form is submitted, the new product will appear in the product list.
        </p>
        <img src="{{asset('images/product/add_new_product.png')}}" alt="Product List"></br>
        <img src="{{asset('images/product/product_list.png')}}" alt="Product List"></br>
        <img src="{{asset('images/product/category_list.png')}}" alt="Product List"></br></br></br>
    </div>

    <div id="service_request_list" class="content" style="display: none;">
        <h3>Service Request List</h3>
        <p>
            The Service Request List allows employees to request assistance when a product is damaged or not functioning properly. 
            They provide a reason for the request, which goes to the admin for approval. Once approved, the request also needs audit 
            approval for added oversight. Auditors can then review pending requests and either approve or reject them, ensuring that 
            all service requests are properly managed and documented.
        </p>
        <img src="{{asset('images/service/request_list.png')}}" alt="Service Request List"></br>
    </div>

    <div id="service_list" class="content" style="display: none;">
        <h3>Service List</h3>
        <p>
            In the Service List, after the audit approves a service request, administrators can attend to the product's needs and 
            document the service details. To input this information, admins navigate to the "Service" menu and click on the "Add New" 
            button. They then enter essential details like the product name, supplier name, quantity, and prices. Additionally, they 
            attach any necessary files related to the service. Once all information is provided, admins submit the form. However, 
            before the process is complete, the request undergoes a two-step approval process, requiring both audit and CMM approvals 
            for finalization. This ensures thorough oversight and accountability in the service request process.
        </p>
        <img src="{{asset('images/service/service_list.png')}}" alt="Service List"></br>
    </div>

    <div id="purchase_invoice_list" class="content" style="display: none;">
        <h3>Purchase Invoice List</h3>
        <p>
            The Purchase Invoice List gives a quick view of all purchase invoices. It helps keep track of buying transactions by 
            showing details like invoice numbers, dates, suppliers, and total amounts in one place. This makes it easy to manage 
            purchases and keep financial records organized.
        </p>
        <p>
            To make a purchase, first, go to the Purchase Invoice menu located within the Purchase module. <b>From there, 
            click on the "Add New" button to access input fields where you can provide purchase information</b>. Fill in all the 
            necessary details and then submit the form to finalize the purchase process. Once submitted, you will be directed to 
            the invoice list, where you can view all the invoices related to your purchases.
        </p>
        
        <img src="{{asset('images/purchase/add_new_purchase.png')}}" alt="Purchase Invoice List"></br>

        <img src="{{asset('images/purchase/purchase_list.png')}}" alt="Purchase Invoice List"></br></br></br>

    </div>
@endsection