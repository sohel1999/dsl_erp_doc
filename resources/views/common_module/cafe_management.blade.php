@extends('layouts.app')
@section('contain')
    <div class="content">
        <h2>POS - My Purchase History</h2>
       
        <div class="dashboard">
            <p>
                This feature enables employees to easily search their food item history by specific dates. 
                They can access a complete list of their purchased food items ordered from DSL Cafe.
            </p>
            
            <img src="{{asset('images/cafe/cafe-purchase-history.png')}}" alt="Dashboard Image"></br>
            <img src="{{asset('images/cafe/history-list.png')}}" alt="Dashboard Image"></br></br></br></br>
        </div>
    </div>
@endsection