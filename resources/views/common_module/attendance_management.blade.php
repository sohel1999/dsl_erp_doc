
@extends('layouts.app')
@section('contain')
    <div class="content">
        <h2>Give Attendance</h2>
       
        <div class="dashboard">
            <p>
                "Give Attendance" allows employees to record their attendance easily. By accessing this feature, 
                employees can log their presence for each workday, providing a straightforward method for tracking their
                attendance within the organization.
            </p>
            <p>
                To mark attendance, employees can access <b>the "Give Attendance" feature within Attendance Management.</b>
                Here, they'll find two buttons: "Check-in" and "Checkout". <b>Employees can conveniently check in using their 
                phones through the ERP system</b>. Additionally, they can check out when their office hours conclude, ensuring 
                accurate recording of their work hours.
            </p>
            <img src="{{asset('images/attendance/give-attendance.png')}}" alt="Give Attendance"></br></br>
        </div>
    </div>
    <div id="attendance_management" class="content" style="display: none;">
        <h3>My Attendance History</h3>
        <p>
            "In My Attendance History, employees can easily check their attendance details day by day. 
            This includes their arrival and departure times, helping them keep track of their work hours."
        </p></br>
        <img src="{{asset('images/attendance/my-attendance-history.png')}}" alt="My Attendance History"></br></br>
        <img src="{{asset('images/attendance/attendance-list.png')}}" alt="My Attendance History"></br></br>
    </div>
@endsection