@extends('layouts.app')
@section('contain')
    <div class="content">
        <h3>My Meal History</h3>
        <p> 
            This feature allows employees to easily search their meal history by specific dates.
            They can access a complete list of their meal purchases, enabling them to review their dining activity over time.</b>.
        </p>
        <img src="{{asset('images/meal/meal-summary.png')}}" alt="My Meal History"></br>
       
        <img src="{{asset('images/meal/meal-history-list.png')}}" alt="Stationery Requisition Request"></br></br></br></br>
    </div>

    <div id="my-meal-list" class="content" style="display: none;">
        <h3>Manage My Meal</h3>
        <p>
            Meal Management gives employees the power to manage their meal requests easily. They can track orders, 
            check past purchases, and adjust preferences as needed. This feature offers employees a complete set of tools 
            for controlling their dining decisions, making meal planning and ordering simple and flexible. 
        </p></br>

        <p>
            To make a meal request, start by accessing <b>"Manage My Meal"</b> within the <b>Meal Management</b> menu. Here, you'll find two buttons:
            <b>"Add a Meal" and "Cancel a Meal"</b>. You can use <b>"Add a Meal" to submit your lunch meal request, specifying the quantity,
            meal date, and then submitting it</b>. Alternatively, if you need to cancel a meal, select <b>"Cancel a Meal" and provide the date and 
            remarks before submitting the request.</b> Afterwards, you can view the list of requests made by you.
        </p>
        <img src="{{asset('images/meal/manage-meal.png')}}" alt="Manage My Meal"></br></br>
    </div>
@endsection