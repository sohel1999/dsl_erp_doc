
@extends('layouts.app')
@section('contain')
    <div class="content">
        <h3>PAYROLL >> MY PAYROLL HISTORY</h3>
       
        <div class="dashboard">
            <p>
                In the <b>"My Payroll History" section under Payroll Management</b>, employees can access a detailed record of their past payslips,
                providing a complete overview of their salary history for each month. Employees can easily view and <b>download their 
                payslips using their unique login password, which corresponds to their employee ID</b>. This feature offers employees easy 
                access to their financial information, ensuring transparency and convenience in managing their payroll records.
            </p>
            
            <img src="{{asset('images/payroll/payroll.png')}}" alt="MY PAYROLL HISTORY"></br></br>
        </div>
    </div>
@endsection