@extends('layouts.app')
@section('contain')
    <div class="content">
        <h3>New Leave Request</h3>
        <p> 
            "New Leave Request" allows employees to submit leave requests efficiently. Within this feature, employees can 
            specify the type of leave, select the dates, provide a reason, and submit the request for approval.
        </p>
        <p>
            To request leave, <b>employees navigate to the Leave module and select "Self Leave Request".</b>
            They complete all required fields, including type of leave, dates, and reason. <b>If an employee requests sick 
            leave for more than one day, attaching documentation becomes mandatory.</b> Once the form is complete, they submit it 
            for approval.
        </p>
        <img src="{{asset('images/leave/new-leave-request.png')}}" alt="New Leave Request"></br>
    </div>

    <div id="my_leave_request" class="content" style="display: none;">
        <h3>My Leave Request List</h3>
        <p>
            My Leave Request List simplifies the process for employees to track their time off. It displays a clear list of their 
            leave requests, indicating whether they are pending approval or have already been approved. This feature empowers 
            employees to stay informed about their leave status, ensuring they can manage their time effectively and plan
            accordingly.
        </p>
        <img src="{{asset('images/leave/leave-list.png')}}" alt="My Leave Request List"></br>
    </div>

    <div id="my_leave_balance" class="content" style="display: none;">
        <h3>My Leave Balance</h3>
        <p>
            My Leave Balance lets employees check how much leave they have left , as determined by the company. 
            It helps them plan their time off wisely.
        </p>
        <img src="{{asset('images/leave/leave-balance.png')}}" alt="My Leave Balance"></br>
    </div>

    <div id="my_annual_leave_status" class="content" style="display: none;">
        <h3>My Annual Leave Status</h3>
        <p>
            My Annual Leave Status provides employees with a comprehensive overview of their annual leave details. 
            It includes information such as the number of days an employee has been present in the year, total annual 
            leave entitlement, days taken for annual leave, and the remaining annual leave days. Additionally, employees 
            can access their Encashment History to view details about encashed leave days and the corresponding amount received. 
            This feature ensures transparency and helps employees manage their annual leave effectively.
        </p>
        <img src="{{asset('images/leave/annual-leave.png')}}" alt="My Annual Leave Status"></br>
    </div>
@endsection