
@extends('layouts.app')
@section('contain')
    <div class="content">
        <h3>My Leave Plan (Calender View)</h3>
       
        <div class="dashboard">
            <p>
                "My Leave Plan" lets employees plan their yearly time off simply. They can schedule their vacation days ahead, making it easy to balance work and personal commitments.
            </p>
            <img src="{{asset('images/leave-plan/leave-plan.png')}}" alt="My Leave Plan"></br>
            <p>
                To use the "My Leave Plan" feature,<b> navigate to the Leave Planner module and select "My Leave Plan"</b>. 
                Click on theb <b> "Add New" button, where you'll need to choose the leave type, specify the purpose of the leave, 
                    provide the start and end dates, and add any remarks.</b> After submitting the form, you'll be able to view your 
                leave plan in a calendar format.
            </p>
            
            <img src="{{asset('images/leave-plan/new-leave-plan.png')}}" alt="My Leave Plan"></br></br>
        </div>
    </div>
@endsection