
@extends('layouts.app')
@section('contain')
    <div class="content">
        <h2>Employee Directory</h2>
       
        <div class="dashboard">
            <p>
                The Employee Directory provides a complete list of all employees within the organization, including their 
                contact information such as phone numbers and email addresses. Employees can easily access this directory to find 
                and connect with their colleagues, facilitating seamless communication and collaboration across the workplace.
            </p>
            
            <img src="{{asset('images/employee-list.png')}}" alt="Employee Directory"></br></br>
        </div>
    </div>
@endsection