@extends('layouts.app')
@section('contain')
    <div id="new-requisition" active class="content">
        <h3>New Requisition List</h3>
        <p> 
            The Requisition Management System simplifies the process for employees to request various items they require for their work. 
            For instance, if an employee needs stationery, they can initiate the request through the <b>"Stationery Requisition" </b>
            section in the sidebar and then proceed to the <b>"New Requisition"</b>. Within the New Stationery Requisition Request interface, they can click on the 
            <b>"Add New Item" button to specify the category of the item they need, then select the specific product and fill up the other star(*) mark option</b> and submit their request. 
            After submission, <b>they await further steps in the requisition process</b>.
        </p>
        <img src="{{asset('images/requisition/new-requisition.png')}}" alt="New Requisition List"></br></br>
        <p> 
            After submitting the request, <b>you can view the list in your requisition on the New Stationery Requisition Request page</b>.
            Here, you can verify whether all items are correct, and if necessary, delete any items. Once all items are added, 
            <b>you must fill out the required date and remarks before submitting the final requisition</b>. After submission, you'll need 
            to wait for <b>admin approval</b>.
         </p>
        <img src="{{asset('images/requisition/requisition-request.png')}}" alt="Stationery Requisition Request"></br></br></br></br>
    </div>

    <div id="my-requisition-list" class="content" style="display: none;">
        <h3>My Requisition List</h3>
        <p>
            In this section, you can view a complete list of all your requisition requests from the day you joined until
            your last day in the office. Here, you can check the status of each requisition request, whether it is pending or approved.
        </p></br>
        <img src="{{asset('images/requisition/requisition-list.png')}}" alt="Team Appraisal List"></br></br>
    </div>
@endsection