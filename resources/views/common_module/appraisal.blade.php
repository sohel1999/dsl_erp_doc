@extends('layouts.app')
@section('contain')
    <div id="appraisal-list" active class="content">
        <h3>My Appraisal List</h3>
        <p> 
           "My Appraisal List" provides a comprehensive overview of the user's performance evaluations.
            It includes evaluations of their performance, achievements, and goals over a certain period.
            This list helps them keep track of their progress, get feedback, and plan for the future, like a report card for work.
        </p>
        <img src="{{asset('images/appraisal/appraisal-list.png')}}" alt="Appraisal List"></br>
    </div>

    <div id="monthly-interface" class="content" style="display: none;">
        <h3>Monthly Appraisal Interface</h3>
        <p>
            The Monthly Appraisal Interface allows managers to search for their team members' names. Within this interface, 
            managers can evaluate their team members' performance marks for the previous month. Additionally, the interface 
            provides a platform for line management to offer feedback and discuss future work plans with team members. Once the
            appraisal process is complete, managers can submit their evaluations.
        <img src="{{asset('images/appraisal/monthly-appraisal-list.png')}}" alt="Monthly Appraisal Interface"></br>
    </div>

    <div id="team-appraisal-list" class="content" style="display: none;">
        <h3>Team Appraisal List</h3>
        <p>
            The "Team Appraisal List" displays a comprehensive overview of the performance evaluations, feedback, and future work 
            plans for each member of a team. This system ensures that only their respective line managers have access to this 
            information, allowing them to assess individual performance, offer constructive feedback, and plan for future tasks 
            and objectives tailored to each team member.</p>
        <img src="{{asset('images/appraisal/team-appraisal-list.png')}}" alt="Team Appraisal List"></br>
    </div>
@endsection