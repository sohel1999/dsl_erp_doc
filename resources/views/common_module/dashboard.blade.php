
@extends('layouts.app')
@section('contain')
    <div class="content">
        <h2>Dashboard</h2>
       
        <div class="dashboard">
            <p>
                This dashboard is the central hub where you can monitor, analyze, and manage all aspects of your business in one place.
                From sales performance to customer insights, it provides real-time data and actionable insights to drive informed decision-making.
            </p>
            
            <img src="{{asset('images/dashboard.png')}}" alt="Dashboard Image"></br></br>
        </div>
    </div>
@endsection