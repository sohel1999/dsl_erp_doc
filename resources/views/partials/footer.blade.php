<footer class="br-footer">
    <div class="footer-left">
        <div class="mg-b-2">Copyright &copy; {{date('Y')}} |<b> DSL. </b>| All Rights Reserved.</div>
        <div></div>
    </div>
    <div class="footer-right d-flex align-items-center">
            <div class="mg-b-2">Proudly Developed by <b>DSL IT Department</b></div>
    </div>
</footer>