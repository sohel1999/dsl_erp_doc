<?php
$url=request()->route()->getName();
?>
<div class="col-md-3 p-0">
    <div class="sidebar">
        <h5><b>Common Modules</b></h5>
        <ul class="sidebar-menu">
            <li class="sidebar-item">
                <a href="{{route('dashboard')}}"> 
                    <i class="fas fa-tachometer-alt"></i> Dashboard
                </a>
            </li>
            <li class="sidebar-item">
                <button class="expand-btn">
                    <i class="menu-item-icon fas fa-award tx-16"></i> Appraisal Management
                </button>
                <ul class="sub-menu">
                    <li><a href="{{route('appraisal')}}" onclick="showContent('appraisal-list')">My Appraisal List</a></li>
                    <li><a href="#" onclick="showContent('monthly-interface')">Monthly Appraisal Interface</a></li>
                    <li><a href="#" onclick="showContent('team-appraisal-list')">Team Appraisal List</a></li>
                </ul>
            </li>
            <li class="sidebar-item">
                <button class="expand-btn">
                    <i class="fas fa-pencil-alt"></i> Stationery Requisition
                </button>
                <ul class="sub-menu">
                    <li><a href="{{route('requisition')}}" onclick="showContent('new-requisition')">New Requisition</a></li>
                    <li><a href="#" onclick="showContent('my-requisition-list')">My Requisition List</a></li>
                </ul>
            </li>
            <li class="sidebar-item">
                <button class="expand-btn">
                    <i class="menu-item-icon fas fa-utensils tx-20"></i> Cafe Management
                </button>
                <ul class="sub-menu">
                    <li><a href="{{route('cafe_management')}}">My Purchase History</a></li>
                </ul>
            </li>
            <li class="sidebar-item">
                <button class="expand-btn">
                    <i class="menu-item-icon fas fa-utensils tx-20"></i> Meal Management
                </button>
                <ul class="sub-menu">
                    <li><a href="{{route('meal_management')}}">My Meal History</a></li>
                    <li><a href="#" onclick="showContent('my-meal-list')">Manage My Meal</a></li>
                </ul>
            </li>

            <li class="sidebar-item">
                <button class="expand-btn">
                    <i class="menu-item-icon fas fa-user-clock tx-16"></i> Attendance Management
                </button>
                <ul class="sub-menu">
                    <li><a href="{{route('attendance_management')}}">Give Attendance</a></li>
                    <li><a href="#" onclick="showContent('attendance_management')">My Attendance History</a></li>
                </ul>
            </li>

            <li class="sidebar-item">
                <button class="expand-btn">
                    <i class="menu-item-icon fas fa-calendar-times tx-20"></i> Leave Management
                </button>
                <ul class="sub-menu">
                    <li><a href="{{route('leave_management')}}">Self Leave Request</a></li>
                    <li><a href="#" onclick="showContent('my_leave_request')">My Leave Request</a></li>
                    <li><a href="#" onclick="showContent('my_leave_balance')">My Leave Blance</a></li>
                    <li><a href="#" onclick="showContent('my_annual_leave_status')">My Annual Leave Status</a></li>
                </ul>
            </li>
            <li class="sidebar-item">
                <button class="expand-btn">
                    <i class="menu-item-icon fas fa-calendar-times tx-20"></i> Leave Planner
                </button>
                <ul class="sub-menu">
                    <li><a href="{{route('leave_planner')}}">My Leave Plan</a></li>
                </ul>
            </li>
            
            <li class="sidebar-item">
                <button class="expand-btn">
                    <i class="menu-item-icon fa fa-money-bill tx-20"></i> Payroll Management
                </button>
                <ul class="sub-menu">
                    <li><a href="{{route('payroll_management')}}">My Payslip History</a></li>
                </ul>
            </li>
            <li class="sidebar-item">
                <a href="{{route('employee_directory')}}">
                    <i class="fas fa-users"></i> Employee Directory
                </a>
            </li>
        </ul>

        <h5><b>HR & ADMIN</b></h5>
        <ul class="sidebar-menu">
            <li class="sidebar-item">
                <button class="expand-btn">
                    <i class="fas fa-archive"></i> Inventory Management
                </button>
                <ul class="sub-menu">
                    <li><a href="{{route('inventory')}}">Product List</a></li>
                    <li><a href="#" onclick="showContent('service_request_list')">Service Request List</a></li>
                    <li><a href="#" onclick="showContent('service_list')">Service List</a></li>
                    <li><a href="#" onclick="showContent('purchase_invoice_list')">Purchase Invoice List</a></li>
                </ul>
            </li>
            <li class="sidebar-item">
                <button class="expand-btn">
                    <i class="fas fa-pen"></i> Stationary Requisition
                </button>
                <ul class="sub-menu">
                    <li><a href="{{route('stationary_requisition')}}">New Requisition</a></li>
                    <li><a href="#" onclick="showContent('my_requisition')">My Requisition</a></li>
                    <li><a href="#" onclick="showContent('pending_requisition')">Pending Requisition</a></li>
                </ul>
            </li>

            <li class="sidebar-item">
                <button class="expand-btn">
                    <i class="fas fa-shopping-cart"></i> Purchase Request
                </button>
                <ul class="sub-menu">
                    <li><a href="{{route('purchase_request')}}">New Purchase Requisition</a></li>
                    <li><a href="#" onclick="showContent('pending_purchase_request')">Pending Requisition</a></li>
                    <li><a href="#" onclick="showContent('update_purchase_request_list')">Updated Purchase Requisition List</a></li>
                    <li><a href="#" onclick="showContent('pending_update_purchase_list')">Pending Updated Purchase List</a></li>
                </ul>
            </li>
            <li class="sidebar-item">
                <button class="expand-btn">
                    <i class="fas fa-archive"></i> Stationary Allocation 
                </button>
                <ul class="sub-menu">
                    <li><a href="{{route('appraisal')}}" onclick="showContent('appraisal-list')">Special Allocation </a></li>
                    <li><a href="#" onclick="showContent('monthly-interface')">Return Allocation Item</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>