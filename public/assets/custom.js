document.addEventListener('DOMContentLoaded', function () {
    var expandBtns = document.querySelectorAll('.expand-btn');

    expandBtns.forEach(function (btn) {
        btn.addEventListener('click', function () {
            var subMenu = this.nextElementSibling;
            subMenu.style.display = subMenu.style.display === 'block' ? 'none' : 'block';
        });
    });
});

function showContent(contentId) {
    var contents = document.querySelectorAll('.content');
    contents.forEach(function(content) {
        content.style.display = 'none';
    });

    var selectedContent = document.getElementById(contentId);
    if (selectedContent) {
        selectedContent.style.display = 'block';
    }
}



